module structures;

import std.range,
       std.typecons;

///
struct Set(T)
{
private:
    T[] m_elements;

public:
    ///
    this(in T[] els)
    {
        foreach (el; els)
        {
            add(el);
        }
    }

    ///
    void add(in T val)
    {
        foreach (x; m_elements)
        {
            if (x == val)
            {
                return;
            }
        }

        m_elements ~= val;
    }

    ///
    void remove(in T val)
    in {
        assert(m_elements.length > 0, "Trying to remove elements from an empty set.");
    }
    body {
        if (m_elements.length > 1) foreach (i, x; m_elements)
        {
            if (x == val)
            {
                foreach (j; i .. m_elements.length - 1)
                {
                    m_elements[i] = m_elements[i + 1];
                }
                break;
            }
        }
        --m_elements.length;
    }

    ///
    void removeIf(bool delegate(T) check)
    {
        foreach (x; m_elements)
        {
            if (check(x))
            {
                remove(x);
            }
        }
    }

    ///
    T[] map(T delegate(T) which)
    {
        T[] res;
        foreach (el; m_elements)
        {
            res ~= which(el);
        }
        return res;
    }

    ///
    void opOpAssign(string op)(in T val)
        if (op == "~")
    {
        add(val);
    }

    ///
    size_t opDollar() const
    {
        return length;
    }

    ///
    T opIndex(in size_t i) const
    {
        return m_elements[i];
    }

    ///
    inout(Set!T) opSlice() inout
    {
        return inout(Set!T)(m_elements);
    }

    ///
    inout(Set!T) opSlice(size_t begin, size_t end) inout
    {
        return inout(Set!T)(m_elements[begin .. end]);
    }

    ///
    int opApply(int delegate(ref T) ops)
    {
        int result = 0;

        foreach (el; m_elements)
        {
            result = ops(el);

            if (result)
            {
                break;
            }
        }

        return result;
    }

    @property
    {
        ///
        size_t length() const
        {
            return m_elements.length;
        }
    }
}

///
struct Queue(T)
{
private:
    T[] m_elements;

public:
    ///
    void push(in T val)
    {
        m_elements ~= val;
    }

    ///
    void pop()
    {
        if (m_elements.length > 1)
        {
            foreach (i; 0 .. m_elements.length - 1)
            {
                m_elements[i] = m_elements[i + 1];
            }
        }
        if (m_elements.length > 0)
        {
            --m_elements.length;
        }
    }

    @property
    {
        ///
        T front() const
        {
            return m_elements[0];
        }

        ///
        bool empty() const
        {
            return m_elements.length == 0;
        }
    }
}

///
struct PriorityQueue(T, char order)
{
private:
    Tuple!(T, float)[] m_elements;

public:
    ///
    void push(T val, float priority)
    {
        m_elements ~= tuple(val, priority);
    }

    ///
    void pop()
    {
        if (m_elements.length > 1)
        {
            size_t best = 0;
            foreach (i, el; m_elements)
            {
                if (mixin ("el[1]" ~ order ~ "m_elements[best][1]"))
                {
                    best = i;
                }
            }

            for (size_t i = best; i < m_elements.length - 1; ++i)
            {
                m_elements[i] = m_elements[i + 1];
            }
        }
        if (m_elements.length > 0)
        {
            --m_elements.length;
        }
    }

    @property
    {
        ///
        T front() const
        {
            size_t best = 0;
            foreach (i, el; m_elements)
            {
                if (mixin ("el[1]" ~ order ~ "m_elements[best][1]"))
                {
                    best = i;
                }
            }

            return m_elements[best][0];
        }

        ///
        bool empty() const
        {
            return m_elements.length == 0;
        }
    }
}
