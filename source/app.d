module app;

import std.exception,
       std.random,
       std.stdio;

import core.time;

import Dgame.Graphic.Color,
       Dgame.Math.Vector2,
       Dgame.Window,
       Dgame.System.Keyboard;

import grid,
       pathfinder;

///
struct Engine
{
private:
    Window m_window = void;
    bool m_running = true;

    Grid m_grid = null;

    void pollEvents()
    {
        Event event;
        while (m_window.poll(&event))
        {
            switch (event.type)
                with (Event.Type)
            {
                case Quit:
                {
                    m_running = false;
                }
                break;

                // controls
                case KeyDown:
                {
                    switch (event.keyboard.key)
                        with (Keyboard.Key)
                    {
                        case Esc:
                        {
                            m_running = false;
                        }
                        break;

                        case C:
                        {
                            m_grid.clear();
                        }
                        break;

                        case D:
                        {
                            m_grid.clear();
                            m_grid.generateDungeon(Vector2i(0, 0), m_grid.size);
                            m_grid.placeAB();
                        }
                        break;

                        case M:
                        {
                            m_grid.clear();
                            m_grid.fill(Grid.Tile.Type.Obstacle, Vector2i(0, 0), m_grid.size);
                            m_grid.generateMaze(Vector2i(
                                uniform(0, m_grid.size.x / 2 - 1) * 2 + 1,
                                uniform(0, m_grid.size.y / 2 - 1) * 2 + 1),
                                                m_grid.size);
                            m_grid.placeAB();
                        }
                        break;

                        case N:
                        {
                            m_grid.clear();
                            m_grid.generatePerlin(Vector2i(0, 0), m_grid.size);
                            m_grid.placeAB();
                        }
                        break;

                        case R:
                        {
                            m_grid.clearPath();
                        }
                        break;

                        case Num1:
                        {
                            if (m_grid.pointA != Vector2i(-1, -1)
                                && m_grid.pointB != Vector2i(-1, -1))
                            {
                                writeln("Testing BFS");

                                foreach (i; 0 .. 30)
                                {
                                    writeln("Test ", i);

                                    MonoTime clock;

                                    m_grid.clearPath();

                                    auto begin = clock.currTime;
                                    auto path = BFS.find(m_grid, m_grid.pointA, m_grid.pointB);
                                    auto end = clock.currTime - begin;

                                    if (path.length < 2)
                                    {
                                        writeln("Path not found");
                                        break;
                                    }

                                    m_grid.setPath(path);

                                    writefln("Time of execution: %d µs", end.total!"usecs");
                                }
                            }
                        }
                        break;

                        case Num2:
                        {
                            if (m_grid.pointA != Vector2i(-1, -1)
                                && m_grid.pointB != Vector2i(-1, -1))
                            {
                                writeln("Testing Dijkstra's search");

                                foreach (i; 0 .. 30)
                                {
                                    writeln("Test ", i);

                                    MonoTime clock;

                                    m_grid.clearPath();

                                    auto begin = clock.currTime;
                                    auto path = Dijkstra.find(m_grid, m_grid.pointA, m_grid.pointB);
                                    auto end = clock.currTime - begin;

                                    if (path.length < 2)
                                    {
                                        writeln("Path not found");
                                        break;
                                    }
                                    m_grid.setPath(path);

                                    writefln("Time of execution: %d µs", end.total!"usecs");
                                }
                            }
                        }
                        break;

                        case Num3:
                        {
                            if (m_grid.pointA != Vector2i(-1, -1)
                                && m_grid.pointB != Vector2i(-1, -1))
                            {
                                writeln("Testing A*");

                                foreach (i; 0 .. 30)
                                {
                                    writeln("Test ", i);

                                    MonoTime clock;

                                    m_grid.clearPath();

                                    auto begin = clock.currTime;
                                    auto path = AStar.find(m_grid, m_grid.pointA, m_grid.pointB);
                                    auto end = clock.currTime - begin;

                                    if (path.length < 2)
                                    {
                                        writeln("Path not found");
                                        break;
                                    }

                                    m_grid.setPath(path);

                                    writefln("Time of execution: %d µs", end.total!"usecs");
                                }
                            }
                        }
                        break;

                        default: break;
                    }
                }
                break;

                default: break;
            }
        }
    }

    void update()
    {
        m_grid.update(m_window);
    }

    void draw()
    {
        m_window.clear();
        m_window.draw(m_grid);
        m_window.display();
    }

public:
    ///
    void run()
    {
        m_grid = new Grid(Vector2i(121, 71), 10);

        m_window = Window(1366, 768, "Path Finders");
        m_window.setClearColor(Color4b.Black);

        while (m_running)
        {
            pollEvents();
            update();
            draw();
        }
    }
}

void main()
{
    Engine app;
    try
    {
        app.run();
    }
    catch (Exception ex)
    {
        stderr.writeln(ex.msg);
    }
}
