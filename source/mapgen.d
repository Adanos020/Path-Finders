module mapgen;

import std.math,
       std.random,
       std.array;

import Dgame.Math.Rect,
       Dgame.Math.Vector2,
       Dgame.Math.Vector3;

alias Vector3d = Vector3!double;

import grid,
       structures : Set;

///
struct MapGenerator
{
private:
    Grid grid;

    int currRegion;
    int[int][int] regions;
    Rect[] rooms;

public:
    ///
    this(Grid grid)
    {
        this.grid = grid;
        reset();
    }

    ///
    void reset()
    {
        currRegion = -1;
        rooms = [];

        foreach (i; 0 .. grid.size.x)
        {
            foreach (j; 0 .. grid.size.y)
            {
                regions[i][j] = -1;
            }
        }
    }

    ///
    void spreadRooms(in Vector2i pos, in Vector2i size)
    {
        foreach (i; 0 .. size.x * size.y / 50)
            with (Grid.Tile.Type)
        {
            Vector2i rsize = Vector2i(uniform(1, 5) * 2 + 1,
                                      uniform(1, 5) * 2 + 1);
            Vector2i rpos = pos + Vector2i(uniform(0, (size.x - rsize.x) / 2) * 2 + 1,
                                           uniform(0, (size.y - rsize.y) / 2) * 2 + 1);

            bool intersects = false;

            auto room = Rect(rpos.x, rpos.y, rsize.x, rsize.y);
            foreach (x; rooms)
            {
                if (room.intersects(x))
                {
                    intersects = true;
                    break;
                }
            }

            if (!intersects)
            {
                rooms ~= room;
                ++currRegion;

                const auto filling = (uniform(0, 2) ? Void : HardTerrain);

                foreach (j; room.x .. room.x + room.width)
                {
                    foreach (k; room.y .. room.y + room.height)
                    {
                        grid.tile(Vector2i(j, k), filling);
                        regions[j][k] = currRegion;
                    }
                }
            }
        }
    }

    ///
    void connectRegions(in Vector2i pos, in Vector2i size)
    {
        Set!Vector2i connectors;

        foreach (room; rooms)
            with (Grid.Tile.Type)
        {
            foreach (x; room.x .. room.x + room.width)
            {
                // top wall
                if ((room.y > 1) && (grid.tile(Vector2i(x, room.y - 1)) == Obstacle)
                    && (regions[x][room.y - 2] != regions[x][room.y])
                    && (regions[x][room.y - 2] != -1)
                    && (regions[x][room.y] != -1))
                {
                    connectors ~= Vector2i(x, room.y - 1);
                    //grid.tile(Vector2i(x, room.y - 1), Grid.Tile.Type.Frontier);
                }
                // bottom wall
                if ((room.y + room.height + 1 < grid.size.y)
                    && (grid.tile(Vector2i(x, room.y + room.height)) == Obstacle)
                    && (regions[x][room.y + room.height - 1] != regions[x][room.y + room.height + 1])
                    && (regions[x][room.y + room.height - 1] != -1)
                    && (regions[x][room.y + room.height + 1] != -1))
                {
                    connectors ~= Vector2i(x, room.y + room.height);
                    //grid.tile(Vector2i(x, room.y + room.height), Grid.Tile.Type.Frontier);
                }
            }
            foreach (y; room.y .. room.y + room.height)
            {
                // left wall
                if ((room.x > 1) && (grid.tile(Vector2i(room.x - 1, y)) == Obstacle)
                    && (regions[room.x - 2][y] != regions[room.x][y])
                    && (regions[room.x - 2][y] != -1)
                    && (regions[room.x][y] != -1))
                {
                    connectors ~= Vector2i(room.x - 1, y);
                    //grid.tile(Vector2i(room.x - 1, y), Grid.Tile.Type.Frontier);
                }
                // right wall
                if ((room.x + room.width + 1 < grid.size.x)
                    && (grid.tile(Vector2i(room.x + room.width, y)) == Obstacle)
                    && (regions[room.x + room.width - 1][y] != regions[room.y + room.width + 1][y])
                    && (regions[room.x + room.width - 1][y] != -1)
                    && (regions[room.y + room.width + 1][y] != -1))
                {
                    connectors ~= Vector2i(room.x + room.width, y);
                    //grid.tile(Vector2i(room.x + room.width, y), Grid.Tile.Type.Frontier);
                }
            }
        }

        int[] merged;
        Set!int openRegions;
        foreach (i; 0 .. currRegion + 1)
        {
            merged ~= i;
            openRegions ~= i;
        }

        while ((openRegions.length > 1) && (connectors.length > 0))
        {
            auto connector = connectors[uniform(0, $)];

            grid.tile(Vector2i(connector.x, connector.y), Grid.Tile.Type.Void);

            int[] toConnect;
            auto directions = [ Vector2i(-1, 0), Vector2i(1, 0),
                                Vector2i(0, -1), Vector2i(0, 1) ];
            foreach (dir; directions)
            {
                if (regions[connector.x + dir.x][connector.y + dir.y] != -1)
                {
                    toConnect ~= regions[connector.x + dir.x][connector.y + dir.y];
                }
            }

            int[] conMer;
            foreach (tc; toConnect)
            {
                conMer ~= merged[tc];
            }

            auto dest = conMer[0];
            auto sources = conMer[1 .. $];

            loop: foreach (i; 0 .. currRegion)
            {
                foreach (s; sources)
                {
                    if (s == merged[i])
                    {
                        merged[i] = dest;
                        break loop;
                    }
                }
            }

            foreach (s; sources)
            {
                openRegions.remove(s);
            }

            connectors.removeIf((pos)
            {
                if (abs((connector - pos).x) < 2
                    || abs((connector - pos).y) < 2)
                {
                    //grid.tile(Vector2i(pos.x, pos.y), Grid.Tile.Type.Current);
                    return true;
                }

                Set!int regs;
                auto directions = [ Vector2i(-1, 0), Vector2i(1, 0),
                                    Vector2i(0, -1), Vector2i(0, 1) ];
                foreach (dir; directions)
                {
                    if (regions[connector.x + dir.x][connector.y + dir.y] != -1)
                    {
                        regs ~= regions[connector.x + dir.x][connector.y + dir.y];
                    }
                }

                if (regs.length > 1)
                {
                    return false;
                }

                if (uniform(0, 20) == 0)
                {
                    grid.tile(Vector2i(pos.x, pos.y), Grid.Tile.Type.Void);
                }

                //grid.tile(Vector2i(pos.x, pos.y), Grid.Tile.Type.Current);
                return true;
            });
        }
    }

    ///
    void removeDeadEnds(in Vector2i pos, in Vector2i size)
    {
        bool done = false;

        while (!done)
        {
            done = true;

            foreach (x; 0 .. grid.size.x)
            {
                foreach (y; 0 .. grid.size.y)
                {
                    if (grid.tile(Vector2i(x, y)) == Grid.Tile.Type.Void)
                    {
                        size_t exits = 0;
                        auto directions = [ Vector2i(-1, 0), Vector2i(1, 0),
                                            Vector2i(0, -1), Vector2i(0, 1) ];
                        foreach (dir; directions)
                        {
                            if (grid.tile(Vector2i(x + dir.x, y + dir.y))
                                != Grid.Tile.Type.Obstacle)
                            {
                                ++exits;
                            }
                        }

                        if (exits == 1)
                        {
                            done = false;
                            grid.tile(Vector2i(x, y), Grid.Tile.Type.Obstacle);
                        }
                    }
                }
            }
        }
    }

    ///
    void generateMaze(in Vector2i pos, in Vector2i size)
    {
        ++currRegion;

        grid.tile(pos, Grid.Tile.Type.Void);

        Vector2i[] cells;
        cells ~= pos;

        Vector2i lastDir = Vector2i.init;

        Vector2i[] directions;
        directions ~= Vector2i(-1,  0);
        directions ~= Vector2i( 0, -1);
        directions ~= Vector2i( 1,  0);
        directions ~= Vector2i( 0,  1);

        while (cells.length > 0)
            with (Grid.Tile.Type)
        {
            Vector2i cell = cells[$ - 1];

            Vector2i[] unmadeCells;

            foreach (dir; directions)
            {
                if (!grid.isOnBorder(cell + dir * 3)
                    && (grid.tile(Vector2i(cell.x + dir.x * 2, (cell.y + dir.y * 2)))
                    == Grid.Tile.Type.Obstacle))
                {
                    unmadeCells ~= dir;
                }
            }

            if (unmadeCells.length > 0)
            {
                Vector2i dir = Vector2i.init;

                bool found = false;
                foreach (x; unmadeCells)
                {
                    if (x == lastDir)
                    {
                        found = true;
                        break;
                    }
                }

                if (found && uniform(0, 2))
                {
                    dir = lastDir;
                }
                else
                {
                    dir = unmadeCells[uniform(0, unmadeCells.length)];
                }

                grid.tile(Vector2i(cell.x + dir.x, cell.y + dir.y), Void);
                regions[cell.x + dir.x][cell.y + dir.y] = currRegion;

                grid.tile(Vector2i(cell.x + dir.x * 2, cell.y + dir.y * 2), Void);
                regions[cell.x + dir.x * 2][cell.y + dir.y * 2] = currRegion;

                cells ~= cell + dir * 2;
                lastDir = dir;
            }
            else
            {
                --cells.length;
                lastDir = Vector2i.init;
            }
        }
    }

    ///
    void generatePerlin(Vector2i pos, Vector2i size)
    {
        PerlinNoise perlin;
        perlin.shuffle();

        foreach (i; pos.x .. pos.x + size.x)
        {
            foreach (j; pos.y .. pos.y + size.y)
            {
                Vector3d arg = Vector3d((cast(double) j / size.x) * 2,
                                        (cast(double) i / size.y) * 2,
                                        0.8);

                const double n = perlin.noise(arg);

                if (n < 0.35)
                {
                    grid.tile(Vector2i(i, j), Grid.Tile.Type.HardTerrain);
                }
                else if (n >= 0.35 && n < 0.6)
                {
                    grid.tile(Vector2i(i, j), Grid.Tile.Type.Void);
                }
                else if (n >= 0.6 && n < 0.8)
                {
                    grid.tile(Vector2i(i, j), Grid.Tile.Type.Obstacle);
                }
            }
        }
    }

    ///
    void placeAB()
    {
        with (Grid.Tile.Type)
        {
            // placing A
            placeA: for (size_t i = 0; (i < grid.size.x) && (i < grid.size.y); ++i)
            {
                foreach (x; 0 .. i)
                {
                    if (grid.tile(Vector2i(x, i)) != Grid.Tile.Type.Obstacle)
                    {
                        grid.tile(Vector2i(x, i), Grid.Tile.Type.PointA);
                        break placeA;
                    }
                }
                foreach (y; 0 .. i)
                {
                    if (grid.tile(Vector2i(i, y)) != Grid.Tile.Type.Obstacle)
                    {
                        grid.tile(Vector2i(i, y), Grid.Tile.Type.PointA);
                        break placeA;
                    }
                }
            }

            // placing B
            placeB: for (size_t i = 0; (i < grid.size.x) && (i < grid.size.y); ++i)
            {
                foreach (x; 0 .. i)
                {
                    if (grid.tile(grid.size - Vector2i(x + 1, i + 1)) != Grid.Tile.Type.Obstacle)
                    {
                        grid.tile(grid.size - Vector2i(x + 1, i + 1), Grid.Tile.Type.PointB);
                        break placeB;
                    }
                }
                foreach (y; 0 .. i)
                {
                    if (grid.tile(grid.size - Vector2i(i + 1, y + 1)) != Grid.Tile.Type.Obstacle)
                    {
                        grid.tile(grid.size - Vector2i(i + 1, y + 1), Grid.Tile.Type.PointB);
                        break placeB;
                    }
                }
            }
        }
    }
}

private struct PerlinNoise
{
private:
    int[] permutation = [
        151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,
        8,99,37,240,21,10,23,190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,
        35,11,32,57,177,33,88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,
        134,139,48,27,166,77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,
        55,46,245,40,244,102,143,54, 65,25,63,161,1,216,80,73,209,76,132,187,208, 89,
        18,169,200,196,135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,
        250,124,123,5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,
        189,28,42,223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 
        43,172,9,129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,
        97,228,251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,
        107,49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
    ];

    double fade(double t) const
    {
        return (t ^^ 3) * (t * (t *6 - 15) + 10);
    }

    double lerp(double t, double a, double b) const
    {
        return a + t * (b - a);
    }

    double grad(int hash, Vector3d pos) const
    {
        const int h = hash & 15;
        const double u = (h < 8) ? pos.x : pos.y;
        const double v = (h < 4) ? pos.y : ((h == 12) || (h == 14) ? pos.x : pos.z);
        return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
    }

public:
    void shuffle()
    {
        foreach (i; 0 .. 256)
        {
            size_t a = uniform(0, permutation.length),
                   b = uniform(0, permutation.length);

            if (a != b)
            {
                int temp = permutation[a];
                permutation[a] = permutation[b];
                permutation[b] = temp;
            }
        }

        auto temp = permutation.dup;
        temp ~= permutation.dup;
        permutation = temp;
    }

    double noise(Vector3d pos)
    {
        Vector3i p = Vector3i(cast(int) floor(pos.x) & 255,
                              cast(int) floor(pos.y) & 255,
                              cast(int) floor(pos.z) & 255);

        p -= Vector3i(cast(int) floor(pos.x),
                      cast(int) floor(pos.y),
                      cast(int) floor(pos.z));

        const double u = fade(pos.x),
                     v = fade(pos.y),
                     w = fade(pos.z);

        const int A  = permutation[  p.x  ] + p.y,
                  AA = permutation[   A   ] + p.z,
                  AB = permutation[ A + 1 ] + p.z,
                  B  = permutation[p.x + 1] + p.y,
                  BA = permutation[   B   ] + p.z,
                  BB = permutation[ B + 1 ] + p.z;

        const double res =
            lerp(w, lerp(v,
                lerp(u, grad(permutation[  AA  ], Vector3d(pos.x    , pos.y    , pos.z)),
                        grad(permutation[  BA  ], Vector3d(pos.x - 1, pos.y    , pos.z))),
                lerp(u, grad(permutation[  AB  ], Vector3d(pos.x    , pos.y - 1, pos.z)),
                        grad(permutation[  BB  ], Vector3d(pos.x - 1, pos.y - 1, pos.z)))),
                lerp(v, lerp(u,
                        grad(permutation[AA + 1], Vector3d(pos.x    , pos.y    , pos.z - 1)),
                        grad(permutation[BA + 1], Vector3d(pos.x - 1, pos.y    , pos.z - 1))),
                lerp(u, grad(permutation[AB + 1], Vector3d(pos.x    , pos.y - 1, pos.z - 1)),
                        grad(permutation[BB + 1], Vector3d(pos.x - 1, pos.y - 1, pos.z - 1)))));

        return (res + 1.0) / 2.0;
    }

}
