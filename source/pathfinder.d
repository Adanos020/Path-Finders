module pathfinder;

import std.stdio;

import Dgame.Math.Vector2;

import grid,
       structures;

///
struct BFS
{
public:
    ///
    static Vector2i[] find(in ref Grid grid, in Vector2i start, in Vector2i end)
    {
        Queue!Vector2i frontier;
        frontier.push(start);

        Vector2i[int] visited;
        int[int] totalCost; // only for measurement

        totalCost[start.x + start.y * grid.size.x] = 0;

        while (!frontier.empty)
        {
            auto current = frontier.front;
            frontier.pop();

            foreach (next; grid.neighbors(current))
            {
                int cost = totalCost[current.x + current.y * grid.size.x]
                         + grid.cost(current, next);
                if (next.x + next.y * grid.size.x !in visited)
                {
                    visited[next.x + next.y * grid.size.x] = current;
                    totalCost[next.x + next.y * grid.size.x] = cost;
                    frontier.push(next);
                }
            }

            if (current == end)
            {
                break;
            }
        }

        // constructing the path
        Vector2i current = end;
        Vector2i[] path = [current];

        while (current != start)
        {
            if (current.x + current.y * grid.size.x !in visited)
            {
                break;
            }
            current = visited[current.x + current.y * grid.size.x];
            path ~= current;
        }

        if (end.x + end.y * grid.size.x in visited)
        {
            writefln("Total cost: %d", totalCost[end.x + end.y * grid.size.x]);
        }

        return path;
    }
}

///
struct Dijkstra
{
public:
    ///
    static Vector2i[] find(in ref Grid grid, in Vector2i start, in Vector2i end)
    {
        PriorityQueue!(Vector2i, '<') frontier;
        frontier.push(start, 0.0);

        Vector2i[int] visited;
        int[int] totalCost;

        visited[start.x + start.y * grid.size.x] = start;
        totalCost[start.x + start.y * grid.size.x] = 0;

        while (!frontier.empty)
        {
            auto current = frontier.front;
            frontier.pop();

            foreach (next; grid.neighbors(current))
            {
                int cost = totalCost[current.x + current.y * grid.size.x]
                         + grid.cost(current, next);
                if ((next.x + next.y * grid.size.x !in totalCost)
                    || (cost < totalCost[next.x + next.y * grid.size.x]))
                {
                    totalCost[next.x + next.y * grid.size.x] = cost;
                    visited[next.x + next.y * grid.size.x] = current;
                    frontier.push(next, cost);
                }
            }

            if (current == end)
            {
                break;
            }
        }

        // constructing the path
        Vector2i current = end;
        Vector2i[] path = [current];

        while (current != start)
        {
            if (current.x + current.y * grid.size.x !in visited)
            {
                break;
            }
            current = visited[current.x + current.y * grid.size.x];
            path ~= current;
        }

        if (end.x + end.y * grid.size.x in visited)
        {
            writefln("Total cost: %d", totalCost[end.x + end.y * grid.size.x]);
        }

        return path;
    }
}

///
struct AStar
{
private:
    static int heuristic(in ref Vector2i from, in ref Vector2i to)
    {
        import std.math : abs;
        return abs(from.x - to.x) + abs(from.y - to.y);
    }

public:
    ///
    static Vector2i[] find(ref Grid grid, in Vector2i start, in Vector2i end)
    {
        PriorityQueue!(Vector2i, '<') frontier;
        frontier.push(start, 0.0);

        Vector2i[int] visited;
        int[int] totalCost;

        visited[start.x + start.y * grid.size.x] = start;
        totalCost[start.x + start.y * grid.size.x] = 0;

        while (!frontier.empty)
        {
            auto current = frontier.front;
            frontier.pop();

            foreach (next; grid.neighbors(current))
            {
                int cost = totalCost[current.x + current.y * grid.size.x]
                         + grid.cost(current, next);
                if ((next.x + next.y * grid.size.x !in totalCost)
                    || (cost < totalCost[next.x + next.y * grid.size.x]))
                {
                    totalCost[next.x + next.y * grid.size.x] = cost;
                    visited[next.x + next.y * grid.size.x] = current;
                    frontier.push(next, cost + heuristic(end, next));
                }
            }

            if (current == end)
            {
                break;
            }
        }

        // constructing the path
        Vector2i current = end;
        Vector2i[] path = [current];

        while (current != start)
        {
            if (current.x + current.y * grid.size.x !in visited)
            {
                break;
            }
            current = visited[current.x + current.y * grid.size.x];
            path ~= current;
        }

        if (end.x + end.y * grid.size.x in visited)
        {
            writefln("Total cost: %d", totalCost[end.x + end.y * grid.size.x]);
        }

        return path;
    }
}
