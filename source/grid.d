module grid;

import std.random,
       std.typecons;

import Dgame.Math.Geometry,
       Dgame.Math.Rect,
       Dgame.Math.Vector2,
       Dgame.Math.Vertex,
       Dgame.Graphic.Color,
       Dgame.Graphic.Drawable,
       Dgame.Graphic.Shape,
       Dgame.System.Keyboard,
       Dgame.System.Mouse;

import mapgen;

///
class Grid : Drawable
{
private:
    MapGenerator m_generator = void;

    Tile[] m_tiles;
    Vector2i m_size;
    Vector2i m_position;
    int m_sideLength;

    int m_A;
    int m_B;

public:
    ///
    this(Vector2i size = Vector2i(1, 1), int sideLength = 10)
    in {
        assert(size.x > 0 && size.y > 0);
    }
    body {
        m_generator = MapGenerator(this);

        m_size       = size;
        m_position   = Vector2i(683 - (sideLength * size.x) / 2,
                                384 - (sideLength * size.y) / 2);
        m_sideLength = sideLength;
        m_A          = -1;
        m_B          = -1;

        for (size_t i = 0; i < size.x * size.y; ++i)
        {
            m_tiles ~= Tile(Tile.Type.Void,
                cast(Vector2f) m_position + Vector2f((i % size.x) * sideLength,
                                                     (i / size.x) * sideLength),
                            sideLength);
        }
    }

    ///
    override void draw(ref const Window window) nothrow @nogc
    {
        foreach (tile; m_tiles)
        {
            window.draw(tile);
        }
    }

    ///
    void update(ref Window window)
    {
        foreach (i, ref tile; m_tiles)
        {
            if (Mouse.getCursorPosition / m_sideLength
                    == cast(Vector2i) (tile.position / m_sideLength))
                with (Tile.Type)
            {
                if (Mouse.isPressed(Mouse.Button.Left)
                    && tile.type == Void)
                {
                    tile.type = Obstacle;
                }
                else if (Mouse.isPressed(Mouse.Button.Right))
                {
                    if (tile.type == PointA)
                    {
                        m_A = -1;
                    }
                    else if (tile.type == PointB)
                    {
                        m_B = -1;
                    }
                    tile.type = Void;
                }
                else if (Mouse.isPressed(Mouse.Button.Middle)
                    && tile.type == Void)
                {
                    tile.type = HardTerrain;
                }
                else if (Keyboard.isPressed(Keyboard.Key.A))
                {
                    if (m_A != -1)
                    {
                        m_tiles[m_A].type = Void;
                    }

                    tile.type = PointA;
                    m_A = cast(int) i;
                }
                else if (Keyboard.isPressed(Keyboard.Key.B))
                {
                    if (m_B != -1)
                    {
                        m_tiles[m_B].type = Void;
                    }

                    tile.type = PointB;
                    m_B = cast(int) i;
                }
            }
        }
    }

    ///
    void generateDungeon(Vector2i pos, Vector2i size)
    {
        fill(Tile.Type.Obstacle, pos, size);

        m_generator.spreadRooms(pos, size);

        for (size_t i = 1; i < m_size.x; i += 2)
        {
            for (size_t j = 1; j < m_size.y; j += 2)
            {
                if (m_tiles[i + j * m_size.x].type == Tile.Type.Obstacle)
                {
                    m_generator.generateMaze(Vector2i(i, j), size);
                }
            }
        }

        m_generator.connectRegions(pos, size);
        m_generator.removeDeadEnds(pos, size);
    }

    ///
    void generateMaze(in Vector2i pos, in Vector2i size)
    {
        m_generator.generateMaze(pos, size);
    }

    ///
    void generatePerlin(in Vector2i pos, in Vector2i size)
    {
        m_generator.generatePerlin(pos, size);
    }

    ///
    void placeAB()
    {
        m_generator.placeAB();
    }

    ///
    void clear()
    {
        m_generator.reset();
        foreach (ref tile; m_tiles)
        {
            tile.type = Tile.Type.Void;
        }
        m_A = -1;
        m_B = -1;
    }

    ///
    void fill(in Tile.Type ttype, in Vector2i pos, in Vector2i size)
    {
        foreach (i; pos.x .. pos.x + size.x)
        {
            foreach (j; pos.y .. pos.y + size.y)
            {
                m_tiles[i + j * m_size.x].type = ttype;
            }
        }
    }

    ///
    void clearPath()
    {
        foreach (ref tile; m_tiles)
            with (Tile.Type)
        {
            if (tile.type == Path || tile.type == Frontier || tile.type == Current)
            {
                tile.type = Void;
            }
            else if (tile.type == HardPath)
            {
                tile.type = HardTerrain;
            }
        }
    }

    ///
    void setPath(Vector2i[] path)
    {
        clearPath();

        foreach (ref step; path)
            with (Tile.Type)
        {
            if (m_tiles[step.x + step.y * m_size.x].type != PointB
                && m_tiles[step.x + step.y * m_size.x].type != PointA)
            {
                if (m_tiles[step.x + step.y * m_size.x].type == HardTerrain)
                {
                    m_tiles[step.x + step.y * m_size.x].type = HardPath;
                }
                else
                {
                    m_tiles[step.x + step.y * m_size.x].type = Path;
                }
            }
        }
    }

    ///
    bool isOnBorder(Vector2i pos) const
    {
        return (pos.x < 0) || (pos.x >= m_size.x) ||
                (pos.y < 0) || (pos.y >= m_size.y);
    }

    ///
    Tile.Type tile(Vector2i pos) const
    {
        if (isOnBorder(pos))
        {
            return Tile.Type.Obstacle;
        }
        return m_tiles[pos.x + pos.y * size.x].type;
    }

    ///
    void tile(Vector2i pos, Tile.Type type)
    {
        if (!isOnBorder(pos))
        {
            if (m_tiles[pos.x + pos.y * m_size.x].type != Tile.Type.PointA
                && m_tiles[pos.x + pos.y * m_size.x].type != Tile.Type.PointB)
            {
                m_tiles[pos.x + pos.y * m_size.x].type = type;
            }

            if (type == Tile.Type.PointA)
            {
                m_A = pos.x + pos.y * m_size.x;
            }
            else if (type == Tile.Type.PointB)
            {
                m_B = pos.x + pos.y * m_size.x;
            }
        }
    }

    ///
    int cost(Vector2i from, Vector2i to) const
    {
        const Vector2i displacement = from - to;
        const int multiplier = (displacement.x != 0 && displacement.y != 0) ? 14 : 10;

        return multiplier * ((m_tiles[to.x + to.y * m_size.x].type == Tile.Type.HardTerrain)
                             ? 5 : 1);
    }

    ///
    Vector2i[] neighbors(Vector2i pos) const
    {
        Vector2i[] result;

        // left
        if (tile(pos + Vector2i(-1, 0)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(-1, 0);
        }

        // top
        if (tile(pos + Vector2i(0, -1)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(0, -1);
        }

        // right
        if (tile(pos + Vector2i(1, 0)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(1, 0);
        }

        // bottom
        if (tile(pos + Vector2i(0, 1)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(0, 1);
        }

        // top left
        if (tile(pos + Vector2i(-1, -1)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(-1, 0)) != Tile.Type.Obstacle   // we check these to block
            && tile(pos + Vector2i( 0, -1)) != Tile.Type.Obstacle) // the diagonal entrance if
        {                                                          // if it's obstructed by
            result ~= pos + Vector2i(-1, -1);                      // its sides
        }

        // top right
        if (tile(pos + Vector2i(1, -1)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(1, 0)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(0, -1)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(1, -1);
        }

        // bottom left
        if (tile(pos + Vector2i(-1, 1)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(-1, 0)) != Tile.Type.Obstacle
            && tile(pos + Vector2i( 0, 1)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(-1, 1);
        }

        // bottom right
        if (tile(pos + Vector2i(1, 1)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(1, 0)) != Tile.Type.Obstacle
            && tile(pos + Vector2i(0, 1)) != Tile.Type.Obstacle)
        {
            result ~= pos + Vector2i(1, 1);
        }

        return result;
    }

    @property
    {
        ///
        Vector2i size() const
        {
            return m_size;
        }

        ///
        Vector2i pointA() const
        {
            if (m_A == -1)
            {
                return Vector2i(-1, -1);
            }
            return Vector2i(m_A % m_size.x, m_A / m_size.x);
        }

        ///
        Vector2i pointB() const
        {
            if (m_B == -1)
            {
                return Vector2i(-1, -1);
            }
            return Vector2i(m_B % m_size.x, m_B / m_size.x);
        }
    }

    ///
    struct Tile
    {
    private:
        Type m_type;
        Shape m_body;

    public:
        alias m_body this;

        ///
        enum Type
        {
            Void,
            HardTerrain,
            PointA,
            PointB,
            Obstacle,
            Path,
            HardPath,
            Current,
            Frontier
        }

        ///
        this(Type t, Vector2f pos, int sideLength)
        {
            m_body = new Shape(Geometry.Quads,
            [
                Vertex(pos),
                Vertex(pos + Vector2f(sideLength,          0)),
                Vertex(pos + Vector2f(sideLength, sideLength)),
                Vertex(pos + Vector2f(         0, sideLength))
            ]);
            m_body.fill = Shape.Fill.Full;

            type = t;
        }

        @property
        {
            ///
            Type type() const
            {
                return m_type;
            }

            ///
            void type(Type t)
            {
                m_type = t;
                switch (m_type)
                    with (Type)
                {
                    case Void:
                    {
                        m_body.setColor(Color4b.Black);
                    }
                    break;

                    case HardTerrain:
                    {
                        m_body.setColor(Color4b.Gray);
                    }
                    break;

                    case PointA:
                    {
                        m_body.setColor(Color4b.Magenta);
                    }
                    break;

                    case PointB:
                    {
                        m_body.setColor(Color4b.Blue);
                    }
                    break;

                    case Obstacle:
                    {
                        m_body.setColor(Color4b.White);
                    }
                    break;

                    case Path:
                    {
                        m_body.setColor(Color4b.Green);
                    }
                    break;

                    case HardPath:
                    {
                        m_body.setColor(Color4b(0, 128, 0));
                    }
                    break;

                    case Current:
                    {
                        m_body.setColor(Color4b(0xaa, 0, 0, 200));
                    }
                    break;

                    case Frontier:
                    {
                        m_body.setColor(Color4b(0xaa, 0xaa, 0, 200));
                    }
                    break;

                    default: assert(0);
                }
            }

            ///
            Vector2i position() const
            {
                return cast(Vector2i) m_body.getVertices[0].position;
            }
        }
    }
}
